FROM php:7.1-apache
MAINTAINER Sajjad Gerami email: sajjad.gerami@gmail.com

########################################################################
################# copy the files over to the container #################
########################################################################

### Changing DocumentRoot
# ENV APACHE_DOCUMENT_ROOT /path/to/new/root
# RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
# RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

##Disable Apache Web Server Signature
COPY config/apache2.conf /etc/apache2/
##Disable PHP Signature
COPY config/php.ini /usr/local/etc/php/

########################################################################
############# installing packages and enabling extensions ##############
########################################################################

# RUN apt-get update -y &&  apt-get install -y libssl-dev
#### BUG: please && two command for production like the above line
RUN apt-get update
RUN apt-get install -y 	libssl-dev \
						libmcrypt-dev\
						libgmp-dev \
						libuv-dev \
						clang \
						make \
						cmake \
						libpcre3-dev \
					#    ffmpeg \
					   	git
	# && docker-php-ext-install gmp mcrypt
RUN apt-get install -y libgmp-dev re2c libmhash-dev libmcrypt-dev file
RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/
RUN docker-php-ext-configure gmp 
RUN docker-php-ext-install gmp
	

# installing mongodb and cassandra driver using PECL
RUN pecl install mongodb \
	#&& pecl install cassandra \
	&& docker-php-ext-enable mongodb 
	#cassandra

# Install datastax php-driver fox cassandra
RUN git clone https://github.com/datastax/php-driver.git /usr/src/datastax-php-driver && \
    cd /usr/src/datastax-php-driver && \
    git checkout --detach v1.2.2 && \
    git submodule update --init && \
    cd ext && \
    ./install.sh && \
echo extension=cassandra.so > /usr/local/etc/php/conf.d/cassandra.ini


# enable apache mod_rewrite
RUN a2enmod rewrite 

# RUN service apache2 restart 
# RUN systemctl restart php7

## Exposing the port to the host
# EXPOSE 8080
# EXPOSE 443
EXPOSE 80

# ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]

# copy the source to the apache directory
# COPY src/ /var/www/html/