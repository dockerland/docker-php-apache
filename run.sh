#!/bin/bash

docker_name=php-app
docker_image_name=php-apache-image
host_volume_path=/opt/src/
container_volume_path=/var/www/html/
app_port_start=80
if [ "$#" -eq  "0" ] 
then
    echo [LOG] "No arguments supplied. setting deploy number to 1"
    deploy=1
else
    echo [LOG] "Arguments supplied. Setting deploy number to "$1
    deploy=$1
fi

echo [LOG] Removing dangling/untagged images
docker rmi $(docker images --quiet --filter "dangling=true")

echo [LOG] Removing $docker_name instances
docker ps -aq -f name=$docker_name | xargs docker rm -f

# echo [LOG] Removing stopped containers
# docker ps -aq --no-trunc | xargs docker rm

echo [LOG] Copy sources into $host_volume_path
mkdir -p $host_volume_path
cp -r ./src $host_volume_path

# echo [LOG] Creating data volume $data_volume_name
# docker volume create $data_volume_name

echo [LOG] building $docker_image_name
docker build -t $docker_image_name . 

for ((i=0; i<deploy; i++)); do
    echo [LOG] Running a container named $docker_name-$i from $docker_image_name
    docker run -d \
        -p $((app_port_start + (i))):80 \
        -v $host_volume_path:$container_volume_path \
        --name $docker_name-$i $docker_image_name
done

echo [LOG] "Successfully Done :-)"
docker ps